package lt.saltyjuice.dragas.hsreplay;

import com.google.gson.Gson;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HSReplayClient {

    private static HSReplayClient instance;
    private final HSReplayAPI client;

    private HSReplayClient()
    {
        Retrofit wrapper = new Retrofit
                .Builder()
                .validateEagerly(true)
                .baseUrl("https://hsreplay.net")
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
        this.client = wrapper.create(HSReplayAPI.class);
    }


    public static HSReplayClient getInstance() {
        if(instance == null)
            instance = new HSReplayClient();
        return instance;
    }

    public Call<HSReplayDataResponse> getCardStatistics(String gameType, int cardid) {
        return client.getSingleCardStatsOverTime(gameType, cardid, "ALL");
    }
}
