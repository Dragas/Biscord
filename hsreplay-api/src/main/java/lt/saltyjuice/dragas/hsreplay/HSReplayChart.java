package lt.saltyjuice.dragas.hsreplay;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.data.category.DefaultCategoryDataset;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class HSReplayChart {

    private final String name;
    private final List<HSReplayDataPoint> data;
    private BufferedImage image;

    private HSReplayChart(HSReplayDataSeries series)
    {
        //this.name = series.getCardId();
        this.data = series.getData();
        this.name = series.getMetadata().keySet().toArray()[0] + " for " + series.getCardId();
    }

    private File getChart(String prefix) throws IOException {
        File imageFile = new File(prefix + "-" + name + "-image.png");
        if(!imageFile.exists()) {
            DefaultCategoryDataset dataset = new DefaultCategoryDataset();
            data
                    .stream()
                    .sorted()
                    .forEachOrdered((point) -> {
                        dataset.addValue(point.getValue(), name, point.getKey());
                    });

            Float max = data.stream().max((it1, it2) -> (int) (it1.getValue() - it2.getValue())).get().getValue();
            Float min = data.stream().min((it1, it2) -> (int) (it1.getValue() - it2.getValue())).get().getValue();
            JFreeChart chart = ChartFactory.createAreaChart(name, "Time", "Popularity", dataset);
            chart.getCategoryPlot().setBackgroundAlpha(0);
            chart.getCategoryPlot().setDomainGridlinesVisible(true);
            chart.getCategoryPlot().getDomainAxis().setCategoryLabelPositions(CategoryLabelPositions.DOWN_90);
            chart.getCategoryPlot().getRangeAxis().setLowerBound(Math.max(0, min - 3));
            chart.getCategoryPlot().getRangeAxis().setUpperBound(Math.min(100, max + 3));
            image = chart.createBufferedImage(1300, 600);
//            PipedInputStream imageInput = new PipedInputStream();
//            PipedOutputStream imageOutput = new PipedOutputStream();
//            imageInput.connect(imageOutput);
            ImageIO.write(image, "png", imageFile);
        }
        return imageFile;
    }

    public static List<File> fromResponse(HSReplayDataResponse response, String prefix) {
        //DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        if(response == null)
            return new ArrayList<>();
        return response
                .getSeries()
                .stream()
                .map(HSReplayChart::new)
                .map((it) -> {
                    try {
                        return it.getChart(prefix);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }


}
