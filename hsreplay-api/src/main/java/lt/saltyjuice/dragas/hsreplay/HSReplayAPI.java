package lt.saltyjuice.dragas.hsreplay;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HSReplayAPI
{
    //https://hsreplay.net/analytics/query/single_card_stats_over_time/?GameType=RANKED_WILD&card_id=41169&RankRange=ALL
    @GET("/analytics/query/single_card_stats_over_time")
    Call<HSReplayDataResponse> getSingleCardStatsOverTime(@Query("GameType") String gameType, @Query("card_id") int cardId, @Query("RankRange") String rankRange);
}
