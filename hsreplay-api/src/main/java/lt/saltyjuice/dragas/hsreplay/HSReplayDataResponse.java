package lt.saltyjuice.dragas.hsreplay;

import com.google.gson.annotations.SerializedName;

import java.time.Instant;
import java.util.List;

public class HSReplayDataResponse {

    @SerializedName("render_as")
    private String renderAs;

    @SerializedName("series")
    private List<HSReplayDataSeries> series;

//    @SerializedName("as_of")
//    private Instant asOf;

    public String getRenderAs() {
        return renderAs;
    }

    public List<HSReplayDataSeries> getSeries() {
        return series;
    }

//    public Instant getAsOf() {
//        return asOf;
//    }

}
