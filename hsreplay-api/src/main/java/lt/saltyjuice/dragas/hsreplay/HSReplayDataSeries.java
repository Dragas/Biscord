package lt.saltyjuice.dragas.hsreplay;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Map;

public class HSReplayDataSeries {

    @SerializedName("card_id")
    private String cardId;

    @SerializedName("metadata")
    private Map<String, String> metadata;

    @SerializedName("data")
    private List<HSReplayDataPoint> data;

    public String getCardId() {
        return cardId;
    }

    public Map<String, String> getMetadata() {
        return metadata;
    }

    public List<HSReplayDataPoint> getData() {
        return data;
    }
}
