package lt.saltyjuice.dragas.hsreplay;

import com.google.gson.annotations.SerializedName;

public class HSReplayDataPoint implements Comparable<HSReplayDataPoint> {

    @SerializedName("x")
    private String key;

    @SerializedName("y")
    private Float value;

    public String getKey() {
        return key;
    }

    public Float getValue() {
        return value;
    }

    @Override
    public int compareTo(HSReplayDataPoint o) {
        return key.compareTo(o.key);
    }
}
